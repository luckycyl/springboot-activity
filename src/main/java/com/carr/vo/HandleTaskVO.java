package com.carr.vo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Map;

/**
 * @author yulin.chen
 * @classname HandleTaskVO
 * @description TODO
 * @date 2020/10/31 13:48
 */
public class HandleTaskVO implements Serializable {
    protected static final long serialVersionUID = 1L;
    @ApiModelProperty("任务ID")
    private String taskId;
    @ApiModelProperty("执行任务参数")
    private Map<String, Object> vars;

    @Override
    public String toString() {
        return "HandleTaskVO{" +
                "taskId='" + taskId + '\'' +
                ", vars=" + vars +
                '}';
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public Map<String, Object> getVars() {
        return vars;
    }

    public void setVars(Map<String, Object> vars) {
        this.vars = vars;
    }
}
