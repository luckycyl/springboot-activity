package com.carr.common.actlistener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

/**
 * @author yulin.chen
 * @classname Listener1
 * @description TODO
 * @date 2020/10/30 17:33
 */
public class Listener1 implements ExecutionListener {
    public void notify(DelegateExecution execution) throws Exception {
        String eventName = execution.getEventName();
        System.out.println("activi 监听器监听到事件" + eventName);
    }

}
