package com.carr.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author yulin.chen
 * @classname SwaggerConfig
 * @description TODO
 * @date 2020/10/30 17:48
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.carr.controller"))
                .paths(PathSelectors.any())
                .build().apiInfo(new ApiInfoBuilder()
                        .title("Activity Swagger2")
                        .description("Activity Swagger2，详细信息......")
                        .version("9.0")
                        .contact(new Contact("陈玉林", "https://blog.csdn.net/qwqw3333333?spm=1010.2135.3001.5113", "569265915@qq.com"))
                        .build());
    }
}
