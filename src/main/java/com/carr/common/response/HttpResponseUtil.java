package com.carr.common.response;

public class HttpResponseUtil {
    private HttpResponseUtil() {
    }

    public static HttpResponse success() {
        ResponseBuilder builder = new ResponseBuilder();
        ResponseDirector director = new ResponseDirector(builder);
        director.construct(HttpResponse.SUCCESS_CODE, HttpResponse.SUCCESS_MSG, null);
        return builder.build();
    }


    public static HttpResponse success(String msg) {
        ResponseBuilder builder = new ResponseBuilder();
        ResponseDirector director = new ResponseDirector(builder);
        director.construct(HttpResponse.SUCCESS_CODE, msg, null);
        return builder.build();
    }


    public static HttpResponse success(String msg, Object data) {
        ResponseBuilder builder = new ResponseBuilder();
        ResponseDirector director = new ResponseDirector(builder);
        director.construct(HttpResponse.SUCCESS_CODE, msg, data);
        return builder.build();
    }

    public static HttpResponse fail() {
        ResponseBuilder builder = new ResponseBuilder();
        ResponseDirector director = new ResponseDirector(builder);
        director.construct(HttpResponse.FAIL_CODE, HttpResponse.FAIL_MSG, null);
        return builder.build();
    }

    public static HttpResponse fail(String msg) {
        ResponseBuilder builder = new ResponseBuilder();
        ResponseDirector director = new ResponseDirector(builder);
        director.construct(HttpResponse.FAIL_CODE, msg, null);
        return builder.build();
    }

    public static HttpResponse fail(String msg, Object data) {
        ResponseBuilder builder = new ResponseBuilder();
        ResponseDirector director = new ResponseDirector(builder);
        director.construct(HttpResponse.FAIL_CODE, msg, data);
        return builder.build();
    }
    public static HttpResponse fail(int code, String msg) {
        ResponseBuilder builder = new ResponseBuilder();
        ResponseDirector director = new ResponseDirector(builder);
        director.construct(code, msg, null);
        return builder.build();
    }

    public static void main(String[] args) {
        System.out.println(HttpResponseUtil.success());
    }
}
