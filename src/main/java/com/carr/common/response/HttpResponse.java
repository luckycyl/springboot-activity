package com.carr.common.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class HttpResponse implements Serializable {
    public static final int NOT_SUPPORT_CONTENT_TYPE_UTF8 = 10000;
    public static final int SUCCESS_CODE = 200;
    public static final int FAIL_CODE = -1;
    public static final String FAIL_MSG = "失败";
    public static final String SUCCESS_MSG = "成功";
    public static final long serialVersionUID = 1L;
    private int code;
    private String msg;
    private Object data;
}
