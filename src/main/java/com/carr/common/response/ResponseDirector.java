package com.carr.common.response;

public  class ResponseDirector {
    private final ResponseBuilder builder;

    public ResponseDirector(ResponseBuilder builder) {
        this.builder = builder;
    }

    public void construct(int code,String msg,Object data){
        builder.code(code);
        builder.msg(msg);
        builder.data(data);
    }
}
