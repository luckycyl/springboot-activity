package com.carr.common.response;

public class ResponseBuilder {
    private HttpResponse httpResponse = new HttpResponse();

    public void code(int code) {
        httpResponse.setCode(code);
    }
    public void msg(String msg) {
        httpResponse.setMsg(msg);
    }
    public void data(Object data) {
        httpResponse.setData(data);
    }

    public HttpResponse build() {
        return httpResponse;
    }
}
