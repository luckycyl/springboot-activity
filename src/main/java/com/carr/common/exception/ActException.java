package com.carr.common.exception;

/**
 * @author yulin.chen
 * @classname ActException
 * @description TODO
 * @date 2020/9/24 10:09
 */
public class ActException extends RuntimeException {
    public ActException(String msg) {
        super(msg);
    }
}
