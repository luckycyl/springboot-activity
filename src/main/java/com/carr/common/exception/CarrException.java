package com.carr.common.exception;

/**
 * @author yulin.chen
 * @classname CarrException
 * @description TODO
 * @date 2020/9/24 10:09
 */
public class CarrException extends RuntimeException {
    public CarrException(String msg) {
        super(msg);
    }
    public CarrException(String msg,Exception e) {
        super(msg,e);
    }
}
