package com.carr.common.util;

import com.carr.api.dto.ActLinkUpTaskCreateDto;
import com.carr.common.exception.CarrException;
import com.carr.common.validate.anno.IsCollection;
import com.carr.common.validate.anno.Required;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class CommonUtil {
    public static void requireNonEmptyArgs(String msg,Object... args) {
        Objects.requireNonNull(args, "传入参数不可为空");
        for (Object arg : args) {
            Objects.requireNonNull(arg,msg);
        }
    }

    /**
     * @description: 验证，要求对象中所有的属性不为空
     * @author: yulin.chen
     * @date: 2020/9/24 10:13
     */
    @SuppressWarnings({"rawtypes","unchecked"})
    public static void requireNonEmptyEntityProperties(Object o) {
        for (Class<?> cl = o.getClass(); cl != Object.class ; cl = cl.getSuperclass()) {
            Field[] fields = cl.getDeclaredFields();
            for (Field field : fields) {
                //如果非必须跳过验证
                if (field.isAnnotationPresent(Required.class)) {
                    Required requiredAnno = field.getAnnotation(Required.class);
                    if (!requiredAnno.value()) {
                        continue;
                    }
                }
                field.setAccessible(true);
                try {
                    Object fieldVal = field.get(o);
                    Objects.requireNonNull(fieldVal, StringUtil.format("字段[{}]的值不可为空",field.getDeclaringClass()+"."+field.getName()));
                    //如果是字符串进一步判断长度
                    if (fieldVal instanceof String) {
                        String s = (String) fieldVal;
                        if (s.trim().length()==0) {
                            throw new CarrException(StringUtil.format("字段[{}]的值的长度不可为0",field.getDeclaringClass()+"."+field.getName()));
                        }
                    }
                    if (field.isAnnotationPresent(IsCollection.class)) {
                        if (!(fieldVal instanceof Collection)) {
                            throw new CarrException(StringUtil.format("字段[{}]的不是集合",field.getDeclaringClass()+"."+field.getName()));
                        }
                        Collection coll = (Collection) fieldVal;
                        if (coll.isEmpty()) {
                            throw new CarrException(StringUtil.format("集合字段[{}]的size不可为0",field.getDeclaringClass()+"."+field.getName()));
                        }
                        coll.forEach(CommonUtil::requireNonEmptyEntityProperties);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    throw new CarrException("获取字段值错误", e);
                }
            }
        }
    }

    public static void main(String[] args) {
        ActLinkUpTaskCreateDto linkUpTaskCreateDto = new ActLinkUpTaskCreateDto();
        linkUpTaskCreateDto.setOwner("陈玉林");
        linkUpTaskCreateDto.setProcessDefinitionId("89");
        linkUpTaskCreateDto.setProcessInstanceId("陈");
        ActLinkUpTaskCreateDto.AssigneeWithPushMsg assigneeWithPushMsg = new ActLinkUpTaskCreateDto.AssigneeWithPushMsg();
        assigneeWithPushMsg.setAssignee("xx");
        assigneeWithPushMsg.setDescription("xx");
        List<ActLinkUpTaskCreateDto.AssigneeWithPushMsg> assigneeWithPushMsgs = new ArrayList<>();
        assigneeWithPushMsgs.add(assigneeWithPushMsg);
        linkUpTaskCreateDto.setAssigneeWithPushMsgs(assigneeWithPushMsgs);
        requireNonEmptyEntityProperties(linkUpTaskCreateDto);
    }
}
