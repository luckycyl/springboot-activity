package com.carr.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

public class Resource {
    public static void main(String[] args) throws IOException {
        InputStream inputStream = getClasspathInputStream("my.properties");
        Properties properties = getPropertiesByInputStream(inputStream);
        System.out.println(getProperty(properties,"name"));
    }

    public static InputStream getClasspathInputStream(String classpath) {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(classpath);
    }

    public static Properties getPropertiesByInputStream(InputStream inputStream) throws IOException {
        Properties properties = new Properties();
        properties.load(inputStream);
        return properties;
    }

    public static Properties getClasspathProperties(String classpath) throws IOException {
        return getPropertiesByInputStream(getClasspathInputStream(classpath));
    }

    public static String getProperty(Properties properties, String key) {
        String s = String.valueOf(properties.get(key));
        try {
            return new String(s.getBytes(),StringUtil.getEncoding(s));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return s;
    }
}
