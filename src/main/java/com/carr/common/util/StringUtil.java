package com.carr.common.util;

import com.carr.common.constant.CharsetConstant;

import java.io.UnsupportedEncodingException;
import java.util.Objects;

public class StringUtil {
    private StringUtil() {
    }

    public static String getEncoding(String s) {
        String encoding = null;
        try {
            if (s.equals(new String(s.getBytes(), CharsetConstant.UTF_8))) {
                encoding = CharsetConstant.UTF_8;
                return encoding;
            } else if (s.equals(new String(s.getBytes(), CharsetConstant.GB2312))) {
                encoding = CharsetConstant.GB2312;
                return encoding;
            } else if (s.equals(new String(s.getBytes(), CharsetConstant.GBK))) {
                encoding = CharsetConstant.GBK;
                return encoding;
            } else if (s.equals(new String(s.getBytes(), CharsetConstant.ISO_8859_1))) {
                encoding = CharsetConstant.ISO_8859_1;
                return encoding;
            } else if (s.equals(new String(s.getBytes(), CharsetConstant.UTF_16))) {
                encoding = CharsetConstant.UTF_16;
                return encoding;
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("input string`s encoding is not common.");
    }

    public static String format(String s, Object... vars){
        String s1 = s.replace("{}", "%s");
        return String.format(s1, vars);
    }

    public static void requireNonEmptyStrings(String msg,String... args) {
        Objects.requireNonNull(args, "传入参数不可为空");
        for (String arg : args) {
            if (Objects.isNull(arg)||arg.trim().length()==0) {
                throw new RuntimeException(msg);
            }
        }
    }
}
