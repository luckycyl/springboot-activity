package com.carr.common.constant;

public interface TaskConstant {
    /**
     * 沟通任务手动取消的标志
     */
    String DELETE_REASON_OWNER = "ownerDelete";
    String DELETE_REASON_COMPLETED = "completed";

    /**
     * 沟通任务标志，分类
     */
    String TASK_CATE_LINK_UP = "link_up_task";
    /**
     * 沟通任务名称
     */
    String TASK_LINK_UP_NAME = "沟通";
}
