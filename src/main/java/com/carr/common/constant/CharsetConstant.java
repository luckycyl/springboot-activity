package com.carr.common.constant;

public interface CharsetConstant {
    String UTF_8 = "UTF-8";
    String GBK = "GBK";
    String GB2312 = "GB2312";
    String ISO_8859_1 = "ISO-8859-1";
    String UTF_16 = "UTF-16";
}
