package com.carr.api.entity;

public class ActHistoryProcessInstanceEntity {
    private String id;
    private String procInstId;
    private String procDefId;
//    BUSINESS_KEY_
//    START_TIME_
//            END_TIME_
//    DURATION_
//            START_USER_ID_
//    START_ACT_ID_
//            END_ACT_ID_
//    SUPER_PROCESS_INSTANCE_ID_
//            DELETE_REASON_
//    TENANT_ID_
//            NAME_


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProcInstId() {
        return procInstId;
    }

    public void setProcInstId(String procInstId) {
        this.procInstId = procInstId;
    }

    public String getProcDefId() {
        return procDefId;
    }

    public void setProcDefId(String procDefId) {
        this.procDefId = procDefId;
    }
}
