package com.carr.api.entity;

public class ActHistoryTaskEntity {
    private String id;
    private String procDefId;
    private String procInstId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProcDefId() {
        return procDefId;
    }

    public void setProcDefId(String procDefId) {
        this.procDefId = procDefId;
    }

    public String getProcInstId() {
        return procInstId;
    }

    public void setProcInstId(String procInstId) {
        this.procInstId = procInstId;
    }
    //    TASK_DEF_KEY_
//            EXECUTION_ID_
//            NAME_
//    PARENT_TASK_ID_
//            DESCRIPTION_
//    OWNER_
//            ASSIGNEE_
//    START_TIME_
//            CLAIM_TIME_
//    END_TIME_
//            DURATION_
//    DELETE_REASON_
//            PRIORITY_
//    DUE_DATE_
//            FORM_KEY_
//    CATEGORY_
//            TENANT_ID_

}
