package com.carr.api.dto;

import lombok.Data;

@Data
public class ActProcessDefDto extends BaseDto {
    private String id;
    private String name;
    private String key;
    private int version;
    private String deploymentId;
    private String resourceName;
}
