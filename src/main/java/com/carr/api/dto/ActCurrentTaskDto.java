package com.carr.api.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ActCurrentTaskDto extends BaseDto {
    private String id;
    private String executionId;
    private String processInstanceId;
    private String processDefinitionId;
    private String name;
    private String parentTaskId;
    private String description;
    private String taskDefinitionKey;
    private String owner;
    private String assignee;
    private Date createTime;
}
