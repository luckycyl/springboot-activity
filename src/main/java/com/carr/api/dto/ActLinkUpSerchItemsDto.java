package com.carr.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ActLinkUpSerchItemsDto extends BaseDto {
    private List<String> instanceIds;
    private List<String> linkUpNames;
}
