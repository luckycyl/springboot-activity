package com.carr.api.dto;

import lombok.Data;

@Data
public class ActHistoryTaskDto extends BaseDto {
    private String id;
    private String processDefinitionId;
    private String taskDefinitionKey;
    private String processInstanceId;
    private String executionId;
    private String name;
    private String parentTaskId;
    private String description;
    private String owner;
    private String assignee;
    private String createTime;
    private String endTime;
    private String deleteReason;
    private String msg;

}
