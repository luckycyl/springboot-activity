package com.carr.api.dto;

import com.carr.common.validate.anno.IsCollection;
import com.carr.common.validate.anno.Required;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author yulin.chen
 * @classname ActLinkUpTaskCreateDto
 * @description 创建沟通活动传输对象
 * @date 2020/9/24 9:32
 */
@Getter
@Setter
public class ActLinkUpTaskCreateDto extends BaseDto {
    //流程定义ID
    private String processDefinitionId;
    //流程实例ID
    private String processInstanceId;
    //沟通发起人
    @Required(value = false)
    private String owner;


    @IsCollection
    private List<AssigneeWithPushMsg> assigneeWithPushMsgs;

    /**
     * 被沟通人以及发送的消息
     */
    @Getter
    @Setter
    public static class AssigneeWithPushMsg{
        //被沟通的人
        private String assignee;
        //发送的沟通消息
        private String description;
    }
}
