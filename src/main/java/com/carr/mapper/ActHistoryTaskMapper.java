package com.carr.mapper;

import com.carr.api.entity.ActHistoryTaskEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ActHistoryTaskMapper {
    /**
     * @description: 补充流程定义ID和流程实例ID
     * @author: yulin.chen
     * @date: 2020/9/23 18:14
     */
    @Update("update act_hi_taskinst set PROC_DEF_ID_ = #{procDefId},proc_inst_id_ = #{procInstId} where id_ = #{id}")
    void updateById(ActHistoryTaskEntity entity);

    /**
     * @description: 获取独一无二流程发起人
     * @author: yulin.chen
     * @date: 2020/9/23 18:16
     */
    @Select("select distinct owner_ from act_hi_taskinst where task_def_key_ = 'link_up_task'")
    List<String> getLinkUpNames();
}
