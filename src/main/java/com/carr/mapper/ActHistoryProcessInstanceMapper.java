package com.carr.mapper;

import com.carr.api.entity.ActHistoryProcessInstanceEntity;
import com.carr.api.entity.ActHistoryTaskEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ActHistoryProcessInstanceMapper {
    /**
     * @description: 获取所有独一无二的实例ID
     * @author: yulin.chen
     * @date: 2020/9/23 18:13
     */
    @Select("select distinct PROC_INST_ID_ as processInstanceId from act_hi_procinst")
    List<String> getInstanceIds();
}
