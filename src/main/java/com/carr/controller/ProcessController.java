package com.carr.controller;

import com.carr.common.response.HttpResponse;
import com.carr.common.response.HttpResponseUtil;
import com.carr.common.util.StringUtil;
import com.carr.service.ActCommonService;
import com.carr.service.ProcessService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/process")
@Api(value = "流程管理")
@Slf4j
public class ProcessController {

    private final RepositoryService repositoryService;
    private final RuntimeService runtimeService;
    private final TaskService taskService;
    private final ProcessService processService;

    public ProcessController(RepositoryService repositoryService, RuntimeService runtimeService, TaskService taskService, ProcessService processService) {
        this.repositoryService = repositoryService;
        this.runtimeService = runtimeService;
        this.taskService = taskService;
        this.processService = processService;
    }

    /**
     * @description: 部署
     * @author: yulin.chen
     * @date: 2020/9/22 15:43
     */
    @PostMapping("/deploy/{fileName}")
    public HttpResponse deploy(@PathVariable(value = "fileName") String fileName) {
        return processService.deploy(fileName);
    }

    @GetMapping("/file/name")
    public HttpResponse getProcessFileName(){
        try {
            URI url = Objects.requireNonNull(ProcessController.class.getClassLoader().getResource("process")).toURI();
            File file = new File(url.getPath());
            File[] files = file.listFiles();
            if (null != files) {
                List<String> fileNameList = new ArrayList<>();
                for (File f : files) {
                    fileNameList.add(f.getName().substring(0,f.getName().indexOf(".")));
                }
                return HttpResponseUtil.success("获取文件名成功",fileNameList);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return HttpResponseUtil.fail(e.getMessage());
        }
        return HttpResponseUtil.fail();
    }

    /**
     * @description: 删除
     * @author: yulin.chen
     * @date: 2020/11/4 18:52
     */
    @DeleteMapping("/delete/{deploymentId}")
    public HttpResponse delete(@PathVariable(value = "deploymentId") String deploymentId) {
        // 默认是false true就是级联删除
        repositoryService.deleteDeployment(deploymentId, true);
        log.info("============= 删除流程成功，流程id为：{}=============", deploymentId);
        return HttpResponseUtil.success("删除流程成功，流程id为" + deploymentId);
    }

    @GetMapping("/process/start")
    public HttpResponse startProcessByProcessDefinitionId(@RequestParam("id") String id) {
        ProcessInstance instance = runtimeService.startProcessInstanceById(id);
//        ProcessInstance instance = runtimeService.startProcessInstanceByKey(id);
        System.out.println(StringUtil.format("============ 启动的实例ID为：{} ============", instance.getId()));

        return HttpResponseUtil.success("创建流程实例成功，实例ID为：" + instance.getProcessInstanceId());
    }


}
