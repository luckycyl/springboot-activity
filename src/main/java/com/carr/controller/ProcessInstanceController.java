package com.carr.controller;

import com.carr.service.ActCommonService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * @author yulin.chen
 * @classname InstanceController
 * @description TODO
 * @date 2020/9/24 13:39
 */
@RestController
@RequestMapping("/instance")
public class ProcessInstanceController {

    private final HistoryService historyService;
    private final ActCommonService actCommonService;

    public ProcessInstanceController(HistoryService historyService, ActCommonService actCommonService) {
        this.historyService = historyService;
        this.actCommonService = actCommonService;
    }

    @GetMapping("/list")
    public Object getProcessInstanceList(@RequestParam(value = "start") int start,@RequestParam(value = "length") int length) {
        HistoricProcessInstanceQuery query = historyService.createHistoricProcessInstanceQuery();
        long count = query.count();
        query.orderByProcessInstanceStartTime().desc();
        List<HistoricProcessInstance> list = query.listPage(start,length);
        HashMap<String, Object> map = new HashMap<>();
        map.put("recordsTotal", count);
        map.put("recordsFiltered", count);
        map.put("data", list);
        return map;
    }
    @GetMapping("/finished/{procInstId}")
    public String judgeProcessInstanceFinished(@PathVariable(value = "procInstId") String procInstId) {
        return actCommonService.judgeProcessInstanceFinished(procInstId) ? "流程已结束" : "流程未结束";
    }
}
