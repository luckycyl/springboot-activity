package com.carr.controller;

import com.carr.api.dto.ActLinkUpTaskCreateDto;
import com.carr.common.response.HttpResponse;
import com.carr.mapper.ActHistoryTaskMapper;
import com.carr.service.ActLinkUpService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/linkup")
public class LinkUpTaskController {
    @Autowired
    private ProcessEngine processEngine;
    @Autowired
    private TaskService taskService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private ActHistoryTaskMapper historyTaskMapper;

    @Autowired
    private ActLinkUpService actLinkUpService;


    /**
     * @description: 创建沟通任务
     * @author: yulin.chen
     * @date: 2020/9/23 13:57
     */
    @PostMapping("/task/create")
    public HttpResponse createLinkUpTask(@RequestBody ActLinkUpTaskCreateDto linkUpTaskCreateDto) {
        return actLinkUpService.createLinkUpTask(linkUpTaskCreateDto);
    }

    /**
     * @param
     * @description: 应答沟通
     * @author: yulin.chen
     * @date: 2020/9/23 17:16
     */
    @PostMapping("/task/ack")
    public HttpResponse ackLinkupTask(@RequestParam(value = "taskId") String taskId,
                                      @RequestParam(value = "assignee") String assignee,
                                      @RequestParam(value = "msg") String msg) {
        return actLinkUpService.ack(taskId, assignee, msg);
    }

    /**
     * @description: 删除沟通任务
     * @author: yulin.chen
     * @date: 2020/9/23 10:44
     */
    @DeleteMapping("/task/delete/{taskId}/{owner}")
    public HttpResponse deleteLinkupTask(@PathVariable(value = "taskId") String taskId, @PathVariable(value = "owner") String owner) {
       return actLinkUpService.deleteLinkupTask(taskId, owner);
    }


    /**
     * @description: 根据流程实例ID，任务发起者，完成原因获取历史任务列表
     * @author: yulin.chen
     * @date: 2020/9/23 14:08
     */
    @GetMapping("/task/list")
    Object getLinkupTaskList(@RequestParam(value = "processInstanceId") String processInstanceId,
                             @RequestParam(value = "owner") String owner,
                             @RequestParam(value = "taskId") String taskId,
                             @RequestParam(value = "start") int start,
                             @RequestParam(value = "length") int length,
                             @RequestParam(value = "deleteReason", required = false) String deleteReason) {
        return actLinkUpService.getLinkupTaskList(taskId,processInstanceId, owner, deleteReason, start, length);
    }

    @GetMapping("/search/items")
    public HttpResponse getSearchItems() {
        return actLinkUpService.getSearchItems();
    }


}
