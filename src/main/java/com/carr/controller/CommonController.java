package com.carr.controller;

import com.carr.common.response.HttpResponse;
import com.carr.common.response.HttpResponseUtil;
import com.carr.common.util.StringUtil;
import com.carr.service.ActCommonService;
import com.carr.vo.HandleTaskVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/common")
@Api(value = "用户管理相关接口")
public class CommonController {

    private final RepositoryService repositoryService;
    private final RuntimeService runtimeService;
    private final TaskService taskService;
    private final ActCommonService actCommonService;

    public CommonController(RepositoryService repositoryService, RuntimeService runtimeService, TaskService taskService, ActCommonService actCommonService) {
        this.repositoryService = repositoryService;
        this.runtimeService = runtimeService;
        this.taskService = taskService;
        this.actCommonService = actCommonService;
    }

    /**
     * @description: 部署
     * @author: yulin.chen
     * @date: 2020/9/22 15:43
     */
    @GetMapping("/deploy")
    public Object deploy(@RequestParam(value = "bpmnName") String bpmnName) {
        DeploymentBuilder deploymentBuilder = repositoryService.createDeployment();
        deploymentBuilder.addClasspathResource(bpmnName);
        Deployment deploy = deploymentBuilder.deploy();
        System.out.println(StringUtil.format("============= 部署的流程ID为：{}=============", deploy.getId()));
        return getProcessDefByDeployId(deploy.getId()).getId();
    }

    @GetMapping("/process/def")
    public ProcessDefinition getProcessDefByDeployId(@RequestParam(value = "deploymentId") String deploymentId) {
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        processDefinitionQuery.deploymentId(deploymentId);
        List<ProcessDefinition> list = processDefinitionQuery.list();
        return list.size() > 0 ? list.get(0) : null;
    }

    @GetMapping("/process/start")
    public HttpResponse startProcessByProcessDefinitionId(@RequestParam("id") String id) {
        ProcessInstance instance = runtimeService.startProcessInstanceById(id);
//        ProcessInstance instance = runtimeService.startProcessInstanceByKey(id);
        System.out.println(StringUtil.format("============ 启动的实例ID为：{} ============", instance.getId()));

        return HttpResponseUtil.success("创建流程实例成功，实例ID为：" + instance.getProcessInstanceId());
    }

    @PostMapping("/task/execu")
    @ApiOperation("添加用户的接口")
    public void execuTask(@RequestBody HandleTaskVO vo) {
        Map<String, Object> vars = vo.getVars();
        String taskId = String.valueOf(vo.getTaskId());
        taskService.complete(taskId, vars);
    }

    @GetMapping("/deploy/delete")
    public void deleteProcess(@RequestParam(value = "id") String id) {
        repositoryService.deleteDeployment(id, true); // 默认是false true就是级联删除
    }

}
