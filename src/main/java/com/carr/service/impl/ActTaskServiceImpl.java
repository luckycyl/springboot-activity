package com.carr.service.impl;

import com.carr.service.ActTaskService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
@Transactional
@Slf4j
public class ActTaskServiceImpl implements ActTaskService {

    private final RuntimeService runtimeService;
    private final TaskService taskService;

    public ActTaskServiceImpl(RuntimeService runtimeService, TaskService taskService) {
        this.runtimeService = runtimeService;
        this.taskService = taskService;
    }


    public Task getTaskById(String taskId) {
        TaskQuery taskQuery = taskService.createTaskQuery();
        taskQuery.taskId(taskId);
        List<Task> list = taskQuery.list();
        if (list.isEmpty()) {
            throw new RuntimeException("改任务不存在,taskId："+taskId);
        }
        return list.get(0);
    }

}
