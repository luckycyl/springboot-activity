package com.carr.service.impl;

import com.carr.common.response.HttpResponse;
import com.carr.common.response.HttpResponseUtil;
import com.carr.service.ProcessService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author yulin.chen
 * @classname ProcessServiceImpl
 * @description TODO
 * @date 2020/11/5 10:36
 */
@Service
@Transactional
@Slf4j
public class ProcessServiceImpl implements ProcessService {
    private final RepositoryService repositoryService;

    public ProcessServiceImpl(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @Override
    public HttpResponse deploy(String fileName) {
        DeploymentBuilder deploymentBuilder = repositoryService.createDeployment();
        deploymentBuilder.addClasspathResource("process/" + fileName + ".bpmn");
        Deployment deploy = deploymentBuilder.deploy();
        log.info("============= 部署的流程ID为：{}=============", deploy.getId());
        return HttpResponseUtil.success("流程部署成功，流程id为" + deploy.getId());
    }
}
