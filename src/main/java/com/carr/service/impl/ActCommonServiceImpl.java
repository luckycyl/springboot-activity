package com.carr.service.impl;

import com.carr.service.ActCommonService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
@Transactional
@Slf4j
public class ActCommonServiceImpl implements ActCommonService {
    private final RuntimeService runtimeService;

    public ActCommonServiceImpl(RuntimeService runtimeService) {
        this.runtimeService = runtimeService;
    }

    @Override
    public boolean judgeProcessInstanceFinished(String procInstId) {
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(procInstId).singleResult();
        if (Objects.nonNull(processInstance)) {
            log.info("=========== processInstance :" + processInstance.toString());
            return false;
        }
        return true;
    }
}
