package com.carr.service;

import org.activiti.engine.task.Task;

public interface ActTaskService {

    Task getTaskById(String taskId);

}
