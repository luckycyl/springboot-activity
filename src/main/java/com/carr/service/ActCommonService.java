package com.carr.service;

import org.activiti.engine.task.Task;

public interface ActCommonService {
    /**
     * @description: 分局流程ID判断流程实例是否执行完毕
     * @author: yulin.chen
     * @date: 2020/9/24 9:58
     */
    boolean judgeProcessInstanceFinished(String procInstId);
}
