package com.carr.service;

import com.carr.api.dto.ActLinkUpTaskCreateDto;
import com.carr.common.response.HttpResponse;

public interface ActLinkUpService {
    /**
     * @description: 获取界面查询条件
     * @author: yulin.chen
     * @date: 2020/9/23 18:08
     * @return
     */
    HttpResponse getSearchItems();

    /**
     * @description: 创建沟通任务
     * @author: yulin.chen
     * @date: 2020/9/24 9:16
     * @return
     */
    HttpResponse createLinkUpTask(ActLinkUpTaskCreateDto linkUpTaskCreateDto);

    /**
     * @description: 应答沟通
     * @author: yulin.chen
     * @date: 2020/9/24 11:16
     * @return
     */
    HttpResponse ack(String taskId, String assignee, String msg);

    /**
     * @description: 删除或者取消沟通任务
     * @author: yulin.chen
     * @date: 2020/9/24 11:26
     * @return
     */
    HttpResponse deleteLinkupTask(String taskId, String owner);

    Object getLinkupTaskList(String taskId, String processInstanceId, String owner, String deleteReason, int start, int length);
}
