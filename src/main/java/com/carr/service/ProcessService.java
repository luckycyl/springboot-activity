package com.carr.service;

import com.carr.common.response.HttpResponse;

/**
 * @author yulin.chen
 * @classname ProcessService
 * @description TODO
 * @date 2020/11/5 10:36
 */
public interface ProcessService {
    /**
     * @description: 部署
     * @author: yulin.chen
     * @date: 2020/11/5 10:38
     */
    HttpResponse deploy(String fileName);
}
