function timestampToTime(timestamp) {
	var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	Y = date.getFullYear() + '-';
	M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
	D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
	h = (date.getHours() < 10 ? '0' + (date.getHours()) : date.getHours()) + ':';
	m = (date.getMinutes() < 10 ? '0' + (date.getMinutes()) : date.getMinutes()) + ':';
	s = (date.getSeconds() < 10 ? '0' + (date.getSeconds()) : date.getSeconds());
	return Y + M + D + h + m + s;
}


var common = {
	timestampToTime: function(timestamp) {
		var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
		Y = date.getFullYear() + '-';
		M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
		D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
		h = (date.getHours() < 10 ? '0' + (date.getHours()) : date.getHours()) + ':';
		m = (date.getMinutes() < 10 ? '0' + (date.getMinutes()) : date.getMinutes()) + ':';
		s = (date.getSeconds() < 10 ? '0' + (date.getSeconds()) : date.getSeconds());
		return Y + M + D + h + m + s;
	},
	msg: {
		success: function(msg) {
			layer.msg(msg, {
				icon: 1,
				time: 3000
			});
		},
		success: function(msg, time) {
			layer.msg(msg, {
				icon: 1,
				time: time
			});
		},
		error: function(msg) {
			layer.msg(msg, {
				icon: 2,
				time: 3000
			});
		},
		error: function(msg, time) {
			layer.msg(msg, {
				icon: 2,
				time: time
			});
		},
		ask: function(msg) {
			layer.msg(msg, {
				icon: 3,
				time: 3000
			});
		},
		ask: function(msg, time) {
			layer.msg(msg, {
				icon: 3,
				time: time
			});
		},
		lock: function(msg) {
			layer.msg(msg, {
				icon: 4,
				time: 3000
			});
		},
		lock: function(msg, time) {
			layer.msg(msg, {
				icon: 4,
				time: time
			});
		}
	},
	alert: {
		success: function(msg) {
			layer.msg(msg, {
				icon: 1,
				time: 3000
			});
		},
		success: function(msg, time) {
			layer.msg(msg, {
				icon: 1,
				time: time
			});
		},
		error: function(msg) {
			layer.msg(msg, {
				icon: 2,
				time: 3000
			});
		},
		error: function(msg, time) {
			layer.msg(msg, {
				icon: 2,
				time: time
			});
		},
		ask: function(msg) {
			layer.msg(msg, {
				icon: 3,
				time: 3000
			});
		},
		ask: function(msg, time) {
			layer.msg(msg, {
				icon: 3,
				time: time
			});
		},
		lock: function(msg) {
			layer.msg(msg, {
				icon: 4,
				time: 3000
			});
		},
		lock: function(msg, time) {
			layer.msg(msg, {
				icon: 4,
				time: time
			});
		}
	},
	tips: {
		up: function(msg, id) {
			layer.tips(msg, id, {
				tips: 1,
				time: 2000
			})
		},
		right: function(msg, id) {
			layer.tips(msg, id, {
				tips: 2,
				time: 2000
			})
		},
		bottom: function(msg, id) {
			layer.tips(msg, id, {
				tips: 3,
				time: 2000
			})
		},
		left: function(msg, id) {
			layer.tips(msg, id, {
				tips: 4,
				time: 2000
			})
		}
	},
	server: {
		hello: function(asyn, method, url, data, callback) {
			$.ajax({
				type: method,
				url: url,
				async: asyn,
				data: data,
				// dataType:'json',
				success: function(res) {
					if (res.code == 200) {
						if (callback) {
							callback(res);
						} else {
							common.msg.success(res.msg);
						}
					} else if(res.code == 10000){
						debugger
						$.ajax({
							type: method,
							url: url,
							async: asyn,
							data: JSON.stringify(data),
							// dataType:'json',
							contentType: "application/json",
							success: function(res) {
								if (res.code == 200) {
									if (callback) {
										callback(res);
									} else {
										common.msg.success(res.msg);
									}
								}else {
									common.msg.error(res.msg);
								}
							},
							error: function(jqXHR) {
								alert("发生错误：" + jqXHR.status);
							}
						});
					}else {
						common.msg.error(res.msg);
					}
				},
				error: function(jqXHR) {
					alert("发生错误：" + jqXHR.status);
				}
			});
		},
		get: function(url, data, callback) {
			common.server.hello(true, "GET", url, data, callback);
		},
		post: function(url, data, callback) {
			common.server.hello(true, "POST", url, data, callback);
		},
		delete: function(url, data, callback) {
			common.server.hello(true, "DELETE", url, data, callback);
		},
		sync: {
			get: function(url, data, callback) {
				common.server.hello(false, "GET", url, data, callback);
			},
			post: function(url, data, callback) {
				common.server.hello(false, "POST", url, data, callback);
			},
			delete: function(url, data, callback) {
				common.server.hello(false, "DELETE", url, data, callback);
			},
		}
	},
	btnDesc(_this){
		common.tips.right($(_this).attr("btnDesc"),_this);
	}
}
